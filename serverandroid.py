import eventlet
import socketio
import cv2
import numpy as np
import os
import time
import json
import base64
import time
import vlc
from PIL import Image
#from playsound import playsound
#import pyglet


def updateListaNomi(id,Newnome):
    print("addcontact...")
    Readed=[]
    with open("idnomi.txt", "r") as filenomi:
        for line in filenomi:
            Readed.append(line)
    TrueList=[x.strip() for x in Readed]
    TrueList[int(id)]=Newnome
    with open("idnomi.txt", "w") as filenomi:
        for i in TrueList:
            filenomi.write("%s\n" % i)
    print(Readed)
    return

def GiveListaNomi():
    Readed=[]
    with open("idnomi.txt", "r") as filenomi:
        for line in filenomi:
            Readed.append(line)
    TrueList=[x.strip() for x in Readed]
    return TrueList

def ADDtoListaNomi(Newnome):
    print("addcontact...")
    Readed=[]
    with open("idnomi.txt", "r") as filenomi:
        for line in filenomi:
            Readed.append(line)
    TrueList=[x.strip() for x in Readed]
    TrueList.append(Newnome)
    with open("idnomi.txt", "w") as filenomi:
        for i in TrueList:
            filenomi.write("%s\n" % i)
    print(Readed)
    return

def LENtoListaNomi():
    print("addcontact...")
    Readed=[]
    with open("idnomi.txt", "r") as filenomi:
        for line in filenomi:
            Readed.append(line)
    TrueList=[x.strip() for x in Readed]
    return (len(TrueList))


path = 'dataset'
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read('trainer/trainer.yml')
cascadePath = "Cascades/haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath)
font = cv2.FONT_HERSHEY_SIMPLEX
# iniciate id counter
id = 0
second_To_end_Riconoscimento=6
NotDesturbVALUE=0
FestaValue = 0

def getImagesAndLabels(path):
    imagePaths = [os.path.join(path,f) for f in os.listdir(path)]
    faceSamples=[]
    ids = []
    for imagePath in imagePaths:
        PIL_img = Image.open(imagePath).convert('L') # grayscale
        img_numpy = np.array(PIL_img,'uint8')
        id = int(os.path.split(imagePath)[-1].split(".")[1])
        faces = faceCascade.detectMultiScale(img_numpy)
        for (x,y,w,h) in faces:
            faceSamples.append(img_numpy[y:y+h,x:x+w])
            ids.append(id)
    return faceSamples,ids

def spioncino():
    time.sleep(2)

def StandardIteration():
    cam = cv2.VideoCapture(0)
    cam.set(3, 640)  # set video widht
    cam.set(4, 480)  # set video height
    # Define min window size to be recognized as a face
    minW = 0.1 * cam.get(3)
    minH = 0.1 * cam.get(4)
    time.sleep(2)
    start_time = time.time()
    NomePersona = ''
    countDS = 0
    global face_idOUT
    riconosciuto = 0
    sconosciuto = 0

    backuppedID = 0


    while True:
        k = cv2.waitKey(10) & 0xff  # Press 'ESC' for exiting video

        if k == 27:
            break

        ret, img = cam.read()
        img = cv2.flip(img, 1)  # Flip vertically
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.2,
            minNeighbors=5,
            minSize=(int(minW), int(minH)),
        )
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            id, confidence = recognizer.predict(gray[y:y + h, x:x + w])
            backuppedID=id
            print("Backuppedid in cam")
            print(backuppedID)

            # If confidence is less them 100 ==> "0" : perfect match
            if (confidence < 100) and (confidence>30):
                names=GiveListaNomi()
                id = names[id]
                confidence = "  {0}%".format(round(100 - confidence))
                now = time.time()
                if ((now - start_time) > second_To_end_Riconoscimento):
                    cv2.imwrite("riconosciuti/now.png", img[y:y + h, x:x + w])
                    riconosciuto=1
                    NomePersona=str(id)
                    break
            else:
                id = "sconosciuto"
                confidence = "  {0}%".format(round(100 - confidence))
                now = time.time()

                # dataset
                if ((now - start_time) > 2) and (countDS<=30):
                    cv2.imwrite("dataset/User." + str(LENtoListaNomi()) + '.' + str(countDS) + ".jpg", gray[y:y + h, x:x + w])
                    countDS+=1

                if ((now - start_time) > second_To_end_Riconoscimento):
                    cv2.imwrite("riconosciuti/now.png", img[y:y + h, x:x + w])
                    sconosciuto=1
                    NomePersona=str(id)

                    #training
                    print("\n [INFO] Training faces. It will take a few seconds. Wait ...")
                    faces, ids = getImagesAndLabels(path)
                    recognizer.train(faces, np.array(ids))
                    # Save the model into trainer/trainer.yml
                    recognizer.write('trainer/trainer.yml')
                    NewNameUNKNOW="sconosciuto"+str(LENtoListaNomi())
                    backuppedID=LENtoListaNomi()
                    ADDtoListaNomi(NewNameUNKNOW)

                    break

            cv2.putText(
                img,
                str(id),
                (x + 5, y - 5),
                font,
                1,
                (255, 255, 255),
                2
            )
            cv2.putText(
                img,
                str(confidence),
                (x + 5, y + h - 5),
                font,
                1,
                (255, 255, 0),
                1
            )

        cv2.imshow('camera', img)

        now = time.time()
        if ((now - start_time) > (second_To_end_Riconoscimento+1)):
            if(sconosciuto==0)and(riconosciuto==0):
                cv2.imwrite("riconosciuti/now.png", img)
                NomePersona="Nessuno"
                break
            else:
                break
    # Do a bit of cleanup
    print("\n [INFO] Exiting Program and cleanup stuff")
    cam.release()
    cv2.destroyAllWindows()

    #compressione
    img = Image.open("riconosciuti/now.png")
    basewidth = 400
    wpercent = (basewidth / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.resize((basewidth, hsize), Image.ANTIALIAS)
    img.save("riconosciuti/now.png")
    print(NomePersona)
    print(backuppedID)
    return str(backuppedID), NomePersona

def encodeandSend(id, nome):
    #encode
    with open("riconosciuti/now.png", "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read()).decode('utf-8')

    jsonTosend = {}
    jsonTosend['id'] = id
    jsonTosend['nome'] = nome
    jsonTosend['jpgstream'] = encoded_string
    json_data = json.dumps(jsonTosend)
    #TEST_IMAGE = 'data:image/png;base64,{}'.format(encoded_string)
    TEST_IMAGE = jsonTosend
    return TEST_IMAGE




sio = socketio.Server(async_mode='eventlet', cors_allowed_origins='*')
#TEST_IMAGE = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAMAAAD04JH5AAAAAXNSR0IB2cksfwAAAH5QTFRFAAAA4ODgt6+5cVp1XEFhb1l0tKu2Z09s/KRY/5xF+6RacVt2/61h+cOa/+W1/9mk/7Np5lxJ6nJa+LOE6nNbxFVPtay3ckVduFJRv7jAc0VdnWplm2dk96Nf/9mj/7Rq/7Jn6nFZ+LeM6GlScURcu1NQckVcaUNew73EmmdkuyH09wAAACp0Uk5TAP//////////////////////////////////////////////////////J0/sFAAAAAlwSFlzAAALEwAACxMBAJqcGAAAAV5JREFUeJzt2ctSwkAQhWHUoEaIGhQVMeBdef8XdCGlMM6pKTOdns1/lqmumm+VOemMDgpnBAAAAAAAAAAAAAAAAAAAAAAAAACAeH54VJlkfNwTcHJam+Rs0hNQ2Zxf1xUAAH0B09KA5jyWi0s3QBvN7KowoL0uDWi9AJN5YcDNOHq33N55AUQW96UBS29A0AseOm9Adi/IBWTfCQAAZANye0E2YL8XrNbqoMdVfC4bsH8VPck+8DyLzxkDdB94EXPWAHkdq7lcwJ9e4A0IesGr7ANv7/E5tz7w8Rmfc+sDwUGVNWDbC3QfGBqQ7AVDA5J3AgAAgwNSvWC52B3/fV+YAZpEH+g2u6/iTWcO+L5i/PYDAuC3H1AAt8/zMD+9oBRg2wvYD/z7O2FqDojvD2Uac0BYPlMBYA2Q+0ORed9fNipif2j/08otAAAAAAAAAAAAAAAAAAAAAAAAAAB8AczYs2A5g3z+AAAAAElFTkSuQmCC'




@sio.event
def connect(sid, environ):
    print("connect")

@sio.on("open door")
def open_door(sid, data):
    print("opening door...")

@sio.on("add_contact")#add contact
def add_contact(sid, id,Newnome):
    updateListaNomi(id,Newnome)

@sio.on("record audio")
def record_audio(sid, data):
    with open("filename.mp3", "wb") as newFile:
        newFile.write(data)
    p = vlc.MediaPlayer(os.path.abspath("filename.mp3"))
    p.play()

@sio.on("citofonataPY")
def citofonataPY(sid, data):
    print("CITOFONAAAATA")
    global NotDesturbVALUE
    if(NotDesturbVALUE==0):
        print("Ringing")
        ID, Nome = StandardIteration()
        #datatrasmit = encodeandSend(ID, Nome)
        #sio.emit('testimage', datatrasmit)
        #if modalitàfesta e sconosciuto= mando testimage
        if (FestaValue==0):#FestaOFF
            datatrasmit = encodeandSend(ID, Nome)
            sio.emit('testimage', datatrasmit)
        else:
            if ("sconosciuto" not in Nome):
                print("amico alla porta, non suono")
            else:
                print("sconosciuto in modalità porta, suono")
                datatrasmit = encodeandSend(ID, Nome)
                sio.emit('testimage', datatrasmit)

    else:
        print("ModalitàNonDisturbareInserita")

@sio.on("Nondisturbare")
def Nondisturbare(sid, data):
    print("NonDisturbare")
    global NotDesturbVALUE
    NotDesturbVALUE=1

@sio.on("disturbare")
def disturbare(sid, data):
    print("Disturba")
    global NotDesturbVALUE
    NotDesturbVALUE=0

@sio.on("FestaON")
def FestaON(sid, data):
    print("FestaON")
    global FestaValue
    FestaValue=1

@sio.on("FestaOFF")
def FestaOFF(sid, data):
    print("FestaOFF")
    global FestaValue
    FestaValue=0

app = socketio.WSGIApp(sio)
eventlet.wsgi.server(eventlet.listen((os.getenv("HOST", default='0.0.0.0'), int(os.getenv("PORT", default='5557')))), app)
