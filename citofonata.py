import socketio
import time

sio = socketio.Client()

@sio.event
def connect():
    print("connesso")
    time.sleep(0.5)
    sio.emit('citofonataPY',"")
    quit()

@sio.event
def disconnect():
    print("disconnected")
    quit()

sio.connect('http://10.0.0.54:5557')
#sio.wait()