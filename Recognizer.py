import cv2
import numpy as np
import os
import socket
import time
import json
import base64
import time
from PIL import Image
import eventlet
import socketio

serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serv.bind(('10.0.0.54', 5557))
serv.listen(5)

conn, addr = serv.accept()

recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read('trainer/trainer.yml')
cascadePath = "Cascades/haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascadePath);
font = cv2.FONT_HERSHEY_SIMPLEX
# iniciate id counter
id = 0
# names related to ids: example ==> Marcelo: id=1,  etc
names = ['None', 'Federico', 'Luca', 'Claudio', 'Z', 'W']
# Initialize and start realtime video capture
cam = cv2.VideoCapture(0)
cam.set(3, 640)  # set video widht
cam.set(4, 480)  # set video height
# Define min window size to be recognized as a face
minW = 0.1 * cam.get(3)
minH = 0.1 * cam.get(4)
riconosciuto=0
sconosciuto=0

jsonTosend = {}
start_time= time.time()
while True:


    ret, img = cam.read()
    img = cv2.flip(img, 1)  # Flip vertically
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.2,
        minNeighbors=5,
        minSize=(int(minW), int(minH)),
    )
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        id, confidence = recognizer.predict(gray[y:y + h, x:x + w])

        # If confidence is less them 100 ==> "0" : perfect match
        if (confidence < 100):
            id = names[id]
            riconosciuto += 1
            confidence = "  {0}%".format(round(100 - confidence))
            now = time.time()
            if ((now - start_time) > 5):
                cv2.imwrite("riconosciuti/User"+ str(id) + str(riconosciuto) + ".jpg", img[y:y + h, x:x + w])

                img = Image.open("riconosciuti/User"+ str(id) + str(riconosciuto) + ".jpg")
                basewidth = 300
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), Image.ANTIALIAS)
                img.save("riconosciuti/User"+ str(id) + str(riconosciuto) + ".jpg")

                with open("riconosciuti/User"+ str(id) + str(riconosciuto) + ".jpg", "rb") as image_file:
                    encoded_string = base64.b64encode(image_file.read())


                jsonTosend['nome'] = str(id)
                jsonTosend['jpgstream'] = encoded_string.decode('ascii')
                json_data = json.dumps(jsonTosend)
                conn.sendall(bytes(json_data,encoding="utf-8"))
                conn.close()
                #break
        else:
            id = "unknown"
            confidence = "  {0}%".format(round(100 - confidence))
            sconosciuto +=1
            now = time.time()
            if ((now - start_time) > 5):
                cv2.imwrite("riconosciuti/User" + str(id) + ".jpg", img[y:y + h, x:x + w])

                img = Image.open("riconosciuti/User" + str(id) + ".jpg")
                basewidth = 300
                wpercent = (basewidth / float(img.size[0]))
                hsize = int((float(img.size[1]) * float(wpercent)))
                img = img.resize((basewidth, hsize), Image.ANTIALIAS)
                img.save("riconosciuti/User" + str(id) + ".jpg")

                with open("riconosciuti/User" + str(id) + ".jpg", "rb") as image_file:
                    encoded_string = base64.b64encode(image_file.read())

                jsonTosend['nome'] = str(id)
                jsonTosend['jpgstream'] = encoded_string.decode('ascii')
                json_data = json.dumps(jsonTosend)
                conn.sendall(bytes(json_data, encoding="utf-8"))
                conn.close()
                #break


        cv2.putText(
            img,
            str(id),
            (x + 5, y - 5),
            font,
            1,
            (255, 255, 255),
            2
        )
        cv2.putText(
            img,
            str(confidence),
            (x + 5, y + h - 5),
            font,
            1,
            (255, 255, 0),
            1
        )

    cv2.imshow('camera', img)

    now = time.time()
    if ((now - start_time) > 7):
        cv2.imwrite("riconosciuti/Nessuno.jpg", img)

        img = Image.open("riconosciuti/Nessuno.jpg")
        basewidth = 300
        wpercent = (basewidth / float(img.size[0]))
        hsize = int((float(img.size[1]) * float(wpercent)))
        img = img.resize((basewidth, hsize), Image.ANTIALIAS)
        img.save("riconosciuti/Nessuno.jpg")

        with open("riconosciuti/Nessuno.jpg", "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())

        jsonTosend['nome'] = "Nessuno"
        jsonTosend['jpgstream'] = encoded_string.decode('ascii')
        json_data = json.dumps(jsonTosend)
        conn.sendall(bytes(json_data, encoding="utf-8"))
        conn.close()
        #break
    k = cv2.waitKey(10) & 0xff  # Press 'ESC' for exiting video


    if k == 27:
        break
    #time.sleep(0.1)
# Do a bit of cleanup
print("\n [INFO] Exiting Program and cleanup stuff")
cam.release()
cv2.destroyAllWindows()